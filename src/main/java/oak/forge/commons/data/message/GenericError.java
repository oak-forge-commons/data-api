package oak.forge.commons.data.message;

import java.util.UUID;

public abstract class GenericError extends Message {

    public GenericError() {
        super();
    }

    public GenericError(UUID receiverId){
        super(receiverId);
    }
}
