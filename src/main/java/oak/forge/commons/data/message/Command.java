package oak.forge.commons.data.message;

import lombok.*;

import java.io.Serializable;
import java.util.UUID;

import static java.lang.String.format;

@Getter
@Setter
public abstract class Command extends Message implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private UUID aggregateId;
    private UUID creatorId;

    public Command() {
        super(PUBLIC_RECEIVER_ID);
        // for JSON parser
    }

    protected Command(String name, UUID aggregateId, UUID creatorId) {
        super(PUBLIC_RECEIVER_ID);
        this.name = name;
        this.aggregateId = aggregateId;
        this.creatorId = creatorId;
    }

    public Command(UUID aggregateId) {
        this(null, aggregateId, null);
    }

    public Command(UUID aggregateId, UUID creatorId) {
        this(null, aggregateId, creatorId);
    }

    public Command withName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        String details = toDetailedString();
        return format("%s (registeredName: %s, aggregateId: %s, correlationId: %s)%s",
                getClass().getName(),
                name,
                aggregateId,
                correlationId,
                details == null || details.isEmpty() ? "" : ": " + details
        );
    }

    protected String toDetailedString() {
        return null;
    }
}
