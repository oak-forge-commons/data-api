package oak.forge.commons.data.message;

import lombok.*;
import oak.forge.commons.data.version.Version;

import java.io.Serializable;
import java.util.UUID;

import static java.lang.String.format;
import static oak.forge.commons.data.version.Version.initial;

@Getter
@Setter
public abstract class Event extends Message implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID id;
    private Version version;
    private UUID aggregateId;
    private UUID creatorId;

    protected Event() {
        super(PUBLIC_RECEIVER_ID);
    }

    public Event(UUID receiverId) {
        super(receiverId);
    }

    protected Event(UUID id, UUID aggregateId, Version version, UUID creatorId) {
        super(PUBLIC_RECEIVER_ID);
        this.id = id;
        this.version = version;
        this.aggregateId = aggregateId;
        this.creatorId = creatorId;
    }

    protected Event(UUID aggregateId, UUID creatorId) {
        this(UUID.randomUUID(), aggregateId, initial(), creatorId);
    }


    public void updateVersion(int version) {
        if (this.version == null) {
            setVersion(initial());
        }
        this.version.set(version);
    }

    public Event correlatingTo(Event other) {
        this.correlationId = other.id;
        return this;
    }

    public Event correlatingTo(UUID correlationId) {
        this.correlationId = correlationId;
        return this;
    }


    @Override
    public String toString() {
        return format("%s (aggregateId: %s, id: %s, version: %s%s)",
                getClass().getName(),
                aggregateId,
                id,
                version,
                correlationId != null ? ", correlationId: " + correlationId : ""
        );
    }

    /**
     * A set of field names useful for serialization process,
     * not to be dependent from runtime class fields introspection
     */
    public static class FieldNames {

        public static final String ID = "id";
        public static final String AGGREGATE_ID = "aggregateId";
        public static final String CREATOR_ID = "creatorId";
        public static final String CORRELATION_ID = "correlationId";
        public static final String CREATION_TIMESTAMP = "creationTimestamp";
        public static final String VERSION = "version";

    }
}
