package oak.forge.commons.data.trait;

public interface Failure {

    void fail(String message);

}
