package oak.forge.commons.data.version;

import java.io.Serializable;

import static java.lang.String.valueOf;

public final class Version implements Serializable, Comparable<Version> {

	private static final long serialVersionUID = 1L;

	private Integer version;

	private Version() {
		this(0);
	}

	private Version(int version) {
		this.version = version;
	}

	public static Version initial() {
		return new Version();
	}

	public static Version of(int value) {
		return new Version(value);
	}

	public int get() {
		return version;
	}

	public void set(int version) {
		this.version = version;
	}

	@Override
	public int compareTo(Version other) {
		return version.compareTo(other.get());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Version version1 = (Version) o;

		return version.equals(version1.version);
	}

	@Override
	public int hashCode() {
		return version.hashCode();
	}

	@Override
	public String toString() {
		return valueOf(version);
	}
}
