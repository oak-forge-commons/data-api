package oak.forge.commons.data.query.filter;

public class FilterException extends RuntimeException {

	private static final long serialVersionUID = -2530315372560688462L;

	public FilterException(String message) {
		super(message);
	}
}
