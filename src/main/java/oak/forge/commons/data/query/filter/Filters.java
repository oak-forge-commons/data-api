package oak.forge.commons.data.query.filter;

import java.util.List;
import java.util.UUID;
import java.util.function.Function;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

import static oak.forge.commons.data.message.Event.FieldNames.AGGREGATE_ID;
import static oak.forge.commons.data.message.Event.FieldNames.VERSION;

public final class Filters {

	private Filters() {
	}

	public static final Filter EMPTY = new OperatorFilter(Operator.NOOP) {
	};
	public static final Filter ALL = EMPTY;

	public static <V> EqFilter<V> eq(String key, V value) {
		return new EqFilter<>(key, value);
	}

	public static AggregateIdFilter aggregateId(UUID aggregateId) {
		return new AggregateIdFilter(aggregateId);
	}

	public static AndFilter and(List<Filter> filters) {
		return new AndFilter(filters);
	}

	public static OrFilter or(List<Filter> filters) {
		return new OrFilter(filters);
	}


	abstract static class OperatorFilter implements Filter {

		private final Operator operator;

		OperatorFilter(Operator operator) {
			this.operator = operator;
		}

		@Override
		public Operator getOperator() {
			return operator;
		}
	}

	abstract static class IterableFilter extends OperatorFilter {

		private final List<Filter> filters;

		IterableFilter(Operator operator, List<Filter> filters) {
			super(operator);
			this.filters = filters;
		}

		public <T> List<T> map(Function<Filter, T> mapper) {
			return filters.stream().map(mapper).collect(toList());
		}

		@Override
		public String toString() {
			return format("%s%s", getOperator(), filters);
		}
	}

	static class ValueFilter<V> extends OperatorFilter {

		private final V value;

		public ValueFilter(Operator operator, V value) {
			super(operator);
			this.value = value;
		}

		public V getValue() {
			return value;
		}
	}

	public static class EqFilter<V> extends OperatorFilter {

		private final String key;
		private final V value;

		EqFilter(String key, V value) {
			super(Operator.EQ);
			this.key = key;
			this.value = value;
		}

		public String getKey() {
			return key;
		}

		public V getValue() {
			return value;
		}

		@Override
		public String toString() {
			return format("%s=%s", key, value);
		}
	}

	static class AggregateIdFilter extends EqFilter<UUID> {

		AggregateIdFilter(UUID aggregateId) {
			super(AGGREGATE_ID, aggregateId);
		}
	}

	static class VersionFilter extends EqFilter<Integer> {

		VersionFilter(int version) {
			super(VERSION, version);
		}
	}

	public static class AndFilter extends IterableFilter {

		AndFilter(Filter... filters) {
			this(asList(filters));
		}

		AndFilter(List<Filter> filters) {
			super(Operator.AND, filters);
		}
	}

	public static class OrFilter extends IterableFilter {

		OrFilter(Filter... filters) {
			this(asList(filters));
		}

		OrFilter(List<Filter> filters) {
			super(Operator.OR, filters);
		}
	}
}
